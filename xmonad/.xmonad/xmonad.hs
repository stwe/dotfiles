{-# LANGUAGE FlexibleContexts #-}
import Data.List
import Data.Maybe (maybeToList)
import Data.Traversable (traverse)

import XMonad
import qualified XMonad.StackSet as W

import XMonad.Actions.NoBorders
import XMonad.Actions.CycleWS
import XMonad.Actions.WindowGo

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops hiding (fullscreenEventHook)
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName

import XMonad.Layout.Fullscreen
import XMonad.Layout.NoBorders
import XMonad.Layout.ResizableTile

import XMonad.Util.EZConfig
import XMonad.Util.NamedWindows (getName)
import XMonad.Util.Run
import XMonad.Util.Scratchpad

import Solarized

-- TODO: gridselect for often-used stuff (screen settings, ...)
-- TODO: bind notebook projector key to screen settings
-- TODO: dmenu setup


main = do
    spawn "killall trayer"
    xmobarPipe <- spawnPipe "xmobar"
    spawn "trayer --edge top --align right --widthtype pixel --width 75 --height 16 --tint 0x002b36 --transparent true --alpha 0 &"

    xmonad $ ewmh defaultConfig {
          borderWidth        = 2
        , focusFollowsMouse  = False
        , clickJustFocuses   = False
        , modMask            = mod4Mask
        , terminal           = myTerminal
        , workspaces         = myWorkspaces
        , layoutHook         = myLayoutHook
        , manageHook         = myManageHook
        , handleEventHook    = fullscreenEventHook
        , logHook            = myLogHook xmobarPipe 
        , startupHook        = setWMName "LG3D"
        , normalBorderColor  = myForegroundColor
        , focusedBorderColor = myHighlightColor
        } `additionalKeys` myKeybindings

-- Layout Hook
myLayoutHook = fullscreenFull $ avoidStruts $ smartBorders (full ||| tiled ||| mirror)
    where
        tiled  = ResizableTall nmaster delta ratio []
        mirror = Mirror tiled
        full   = Full

        nmaster = 1         -- Number of master windows 
        delta   = 3/100     -- Change when resizing windows
        ratio   = 1/2       -- Default ratio of master/slave

-- Manage Hook
myManageHook :: ManageHook
myManageHook = manageDocks <+> scratchpadHook <+>
    (composeAll . concat $
    [ 
      [className =? c --> doF (W.shift "2") | c <- apps2]
    , [className =? c --> doF (W.shift "5") | c <- apps5]
    , [className =? c --> doF (W.shift "8") | c <- apps8]
    , [className =? c --> doCenterFloat     | c <- appsFloat]
    , [className =? c --> doFullFloat       | c <- appsFull]
    ]) <+> fullscreenManageHook

    where
        apps2     = ["Navigator", "Firefox", "Chromium"]
        apps5     = ["Thunderbird"]
        apps8     = ["Spotify"]
        appsFloat = [" ", "Keepassx", "mpv", "Mmex"] -- matplotlib has " " as WM_CLASS
        appsFull  = ["Plugin-container"]
        scratchpadHook = scratchpadManageHook $ W.RationalRect l t w h

        -- Dimensions of scratchpad
        l = 0.025
        t = 0.02
        w = 0.95
        h = 0.80


-- Terminal
myTerminal = "urxvtcc"

-- Dmenu
myDmenuCmd = intercalate " " [
          "dmenu_run"
        , "-fn", "'Inconsolata:size=11'"
        , "-nb", wrap myBackgroundColor
        , "-nf", wrap myForegroundColor
        , "-sb", wrap myBackgroundColor
        , "-sf", wrap myHighlightColor
    ]
    where
        wrap s = "'" ++ s ++ "'"

-- Workspaces
myWorkspaces = map show [1 .. 9 :: Int]

-- Keybindings
xF86AudioLowerVolume = 0x1008ff11
xF86AudioRaiseVolume = 0x1008ff13
xF86AudioMute        = 0x1008ff12
xF86AudioPlay        = 0x1008ff14
xF86AudioNext        = 0x1008ff17
xF86AudioPrev        = 0x1008ff16

myKeybindings = 
    [
    -- XMonad.Actions.CycleWS bindigns
      ((mod4Mask, xK_Tab), toggleWS)
    , ((mod1Mask, xK_Tab), nextScreen)

    -- Maximize, toggle struts/borders
    , ((mod4Mask, xK_b), sendMessage ToggleStruts)
    , ((mod4Mask, xK_g), withFocused toggleBorder)

    -- Shrink/Expand tiled windows
    , ((mod4Mask .|. shiftMask, xK_h), sendMessage MirrorShrink)
    , ((mod4Mask .|. shiftMask, xK_l), sendMessage MirrorExpand)

    -- Application Launchers
    , ((mod4Mask .|. controlMask, xK_t), runOrRaise "thunderbird" (className =? "Thunderbird"))
    , ((mod4Mask .|. controlMask, xK_f), runOrRaise "chromium"     (className =? "Chromium"))
    , ((mod4Mask .|. controlMask, xK_e), spawn "dolphin")
    , ((mod4Mask .|. controlMask, xK_q), spawn "xmonad --restart")
    , ((mod4Mask, xK_p), spawn myDmenuCmd)
    , ((mod4Mask, xK_Return), spawn myTerminal)
    -- TODO: launch kde screen settings

    -- Scratchpad
    , ((mod4Mask, xK_s), scratchpadSpawnActionTerminal myTerminal)

    -- Multimedia Keys
    , ((0, xF86AudioLowerVolume), spawn "/usr/bin/pulseaudio-ctl down")
    , ((0, xF86AudioRaiseVolume), spawn "/usr/bin/pulseaudio-ctl up")
    , ((0, xF86AudioMute),        spawn "/usr/bin/pulseaudio-ctl mute")
    , ((0, xF86AudioPlay),        spawn "/usr/bin/playerctl play-pause")
    , ((0, xF86AudioNext),        spawn "/usr/bin/playerctl next")
    , ((0, xF86AudioPrev),        spawn "/usr/bin/playerctl previous")
    ]

-- xmobar
myPP = defaultPP {
            ppCurrent = xmobarColor myHighlightColor "" . wrap "[" "]" . filterNSP
          , ppVisible = wrap "(" ")" . filterNSP
          , ppHidden  = filterNSP
          , ppUrgent  = xmobarColor myHighlightColor2   "" . wrap "*" "" . filterNSP
          , ppSep     = " | "
          , ppLayout  = (\x -> case x of 
                            "ResizableTall"        -> "T"
                            "Mirror ResizableTall" -> "M"
                            "Full"                 -> "F"
                            _                               -> x
                        )
          , ppExtras = [logTitles]
          , ppTitle   = xmobarColor myHighlightColor ""
          , ppOrder   = \(ws:l:t:o) -> ws : l : t : o

    } where
        filterNSP = (\x -> case x of 
                        "NSP" -> "" 
                        _     -> x
                    )

        logTitles :: X (Maybe String)
        logTitles = withWindowSet $ fmap (Just . unwords)
                                  . traverse (fmap show . getName)
                                  . (\ws -> W.index ws \\ maybeToList (W.peek ws))


myLogHook pipe = dynamicLogWithPP $ myPP { ppOutput = hPutStrLn pipe }

