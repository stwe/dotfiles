set nocompatible
filetype off

" Plugins
if has('win32') || has('win64')
    set runtimepath-=~\vimfiles
    set runtimepath^=~\.vim
    set runtimepath-=~\vimfiles/after
    set runtimepath+=~\.vim/after
    set runtimepath+=~\vimfiles/bundle/Vundle.vim
    " sessions path
    set directory=~/vimfiles/sessions
else
    set rtp+=~/.vim/bundle/Vundle.vim
    " sessions path
    set directory=~/.vim/sessions
endif
call vundle#begin()

Plugin 'VundleVim/Vundle.vim'

Plugin 'fholgado/minibufexpl.vim'
Plugin 'scrooloose/nerdtree'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'bling/vim-airline'

Plugin 'godlygeek/tabular'
Plugin 'tpope/vim-commentary'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-repeat'

Plugin 'altercation/vim-colors-solarized'

"Plugin 'gerw/vim-latex-suite'
Plugin 'lervag/vimtex'
Plugin 'hdima/python-syntax'
Plugin 'nathangrigg/vim-beancount'

Plugin 'file://~/.vim/bundle/vim-syntax-iecwin'

call vundle#end()

" Colors and Highlighting
set background=dark

set t_Co=256
let g:solarized_termtrans=0
let g:solarized_bold=1
let g:solarized_underline=1
let g:solarized_italic=1

colorscheme solarized
if has("gui_running")
    set background=light
    colorscheme solarized
    set guioptions-=m
    set guioptions-=t
    set guioptions-=T
    set guioptions-=r
    set guioptions-=R
    set guioptions-=l
    set guioptions-=L
    set guioptions-=b
    set nocursorline
    set guifont=Consolas:h11:cANSI:qDRAFT
    set lines=50 columns=100
endif

" Disable bell sound
set noeb vb t_vb=


" Backspace on windows
set backspace=indent,eol,start

" Enable syntax highlighting
autocmd BufEnter * :syntax sync fromstart
filetype plugin indent on
syntax on

" Indentation settings
set smarttab
set smartindent
set shiftwidth=3
set tabstop=3
set softtabstop=3
set tw=120
set expandtab

set display+=lastline

" Save as UTF-8
set enc=utf-8

set number
set mouse=a

" Folding
set foldmethod=marker
 
" Searching
set smartcase
set hlsearch
set incsearch
set ignorecase

" Bindkeys
let mapleader=","
let maplocalleader=","
nmap <C-h> <C-W>h
nmap <C-j> <C-W>j
nmap <C-k> <C-W>k
nmap <C-l> <C-W>l

nmap <F10> :NERDTreeToggle<CR>
nmap <F8>  :make<CR>

map <Esc>[27;5;9~ <C-Tab>

noremap <C-n> :MBEbn<CR>
noremap <C-b> :MBEbp<CR>
noremap <C-Tab> :MBEbn<CR>
noremap <C-S-Tab> :MBEbp<CR>

nmap t <C-]>
nmap T <C-t>

nmap ,, :noh<CR>
map <leader>gf :e <cfile><cr>

imap <C-space> <C-x><C-n>

" Using sudo after opening vim
cmap w!! w !sudo tee % > /dev/null

"
" Plugins
"

" minibufexpl
let g:miniBufExplMapWindowNavVim=1
let g:miniBufExplMapWindowNavArrows=1
let g:miniBufExplMapCTabSwitchBufs=1

set hidden

" CtrlP
let g:ctrlp_map = "<c-p>"
let g:ctrlp_cmd = "CtrlP"

" latexsuite
let g:tex_flavour="latex"
set wildignore=*.log,*.aux,*.dvi,*.aut,*.aux,*.bbl,*.blg,*.dvi,*.fff,*.log,*.out,*.pdf,*.ps,*.toc,*.tt
let g:Tex_DefaultTargetFormat = "pdf"
let g:Tex_MultipleCompileFormats = "div,pdf"
let g:Tex_Env_frame = "\\begin{frame}\<CR>\\frametitle{<+Frame title+>}\<CR><+Contents+>\<CR>\\end{frame}"
let g:Tex_Com_siu = "\\SI{<+value+>}{<+dimension+>}<++>"

" Python Syntax highlighting
let python_highlight_all=1
let python_highlight_exceptions=1
let python_highlight_builtins=0
let python_highlight_builtins_objs=1
let python_slow_sync=1

" NERDTree
let NERDTreeIgnore = ['\~$', '\.pyc$', '\.o$'] 
let NERDTreeMinimalUI = 1

" iecwin
au BufRead,BufNewFile *.inp set foldmethod=syntax 
au BufRead,BufNewFile *.inp setlocal commentstring=//\ %s
"au BufRead,BufWinEnter *.inp normal zR

